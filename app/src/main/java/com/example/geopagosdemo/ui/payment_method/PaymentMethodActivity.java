package com.example.geopagosdemo.ui.payment_method;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.geopagosdemo.R;
import com.example.geopagosdemo.model.PaymentMethod;
import com.example.geopagosdemo.ui.utils.IOnClickListenerRecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PaymentMethodActivity extends AppCompatActivity {

    public final static String PAYMENTS = "payments";
    public final static String PAYMENT_SELECTED = "payment_selected";

    private ArrayList<PaymentMethod> paymentMethods;

    @BindView(R.id.rv_forma_pago)
    RecyclerView rvFormaPago;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_method);
        ButterKnife.bind(this);

        paymentMethods = getIntent().getParcelableArrayListExtra(PAYMENTS);

        RecyclerView.LayoutManager layoutManagerSelected_fc = new LinearLayoutManager(this);
        rvFormaPago.setLayoutManager(layoutManagerSelected_fc);

        PaymentMethodAdapter adapter = new PaymentMethodAdapter(this, paymentMethods);
        adapter.setOnClick(new IOnClickListenerRecyclerView() {
            @Override
            public void OnItemClick(int position) {
                Log.e("PM Selected: ", Integer.toString(position));
                Intent intent = getIntent();
                intent.putExtra(PAYMENT_SELECTED, paymentMethods.get(position));
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        rvFormaPago.setAdapter(adapter);
    }
}

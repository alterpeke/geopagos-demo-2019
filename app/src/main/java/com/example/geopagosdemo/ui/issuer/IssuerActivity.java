package com.example.geopagosdemo.ui.issuer;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.geopagosdemo.R;
import com.example.geopagosdemo.model.Issuer;
import com.example.geopagosdemo.ui.utils.IOnClickListenerRecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class IssuerActivity extends AppCompatActivity {

    public final static String ISSUERS = "issuers";
    public final static String ISSUER_SELECTED = "issuer_selected";

    private ArrayList<Issuer> issuers;

    @BindView(R.id.rv_forma_pago)
    RecyclerView rvIssuers;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_method);
        ButterKnife.bind(this);

        issuers = getIntent().getParcelableArrayListExtra(ISSUERS);

        RecyclerView.LayoutManager layoutManagerSelected_fc = new LinearLayoutManager(this);
        rvIssuers.setLayoutManager(layoutManagerSelected_fc);

        IssuerAdapter adapter = new IssuerAdapter(this, issuers);
        adapter.setOnClick(new IOnClickListenerRecyclerView() {
            @Override
            public void OnItemClick(int position) {
                Log.e("Selected: ", Integer.toString(position));
                Intent intent = getIntent();
                intent.putExtra(ISSUER_SELECTED, issuers.get(position));
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        rvIssuers.setAdapter(adapter);
    }
}

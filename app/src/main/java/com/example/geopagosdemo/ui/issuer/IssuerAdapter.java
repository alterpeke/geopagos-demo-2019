package com.example.geopagosdemo.ui.issuer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.geopagosdemo.R;
import com.example.geopagosdemo.model.Issuer;
import com.example.geopagosdemo.ui.utils.IOnClickListenerRecyclerView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class IssuerAdapter extends RecyclerView.Adapter<IssuerAdapter.ViewHolder> {

    private ArrayList<Issuer> issuers;
    private Context context;
    private IOnClickListenerRecyclerView onClick;

    public IssuerAdapter(Context context, ArrayList<Issuer> issuers) {
        this.context=context;
        this.issuers=issuers;
    }

    @NonNull
    @Override
    public IssuerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //Create a new item view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_issuer, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull IssuerAdapter.ViewHolder holder, int position) {
        Picasso.with(context).load(issuers.get(position).getSecureThumbnail())
                .into(holder.img);
        holder.bankName.setText(issuers.get(position).getName());
        holder.bankName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClick.OnItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return issuers.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView img;
        private TextView bankName;

        public ViewHolder(View view) {
            super(view);
            img = view.findViewById(R.id.img_bank);
            bankName = view.findViewById(R.id.tv_bankName);

        }
    }

    public void setOnClick(IOnClickListenerRecyclerView onClick) {
        this.onClick = onClick;
    }
}

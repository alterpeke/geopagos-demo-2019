package com.example.geopagosdemo.ui.payment_method;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.geopagosdemo.R;
import com.example.geopagosdemo.model.PaymentMethod;
import com.example.geopagosdemo.ui.utils.IOnClickListenerRecyclerView;

import java.util.ArrayList;

public class PaymentMethodAdapter extends RecyclerView.Adapter<PaymentMethodAdapter.ViewHolder> {

    private ArrayList<PaymentMethod> paymentMethods;
    private Context context;
    private IOnClickListenerRecyclerView onClick;

    public PaymentMethodAdapter(Context context, ArrayList<PaymentMethod> paymentMethods) {
        this.context = context;
        this.paymentMethods = paymentMethods;
    }

    @NonNull
    @Override
    public PaymentMethodAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        //Create a new item view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_payment_method, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull PaymentMethodAdapter.ViewHolder holder, int position) {
        holder.name.setText(paymentMethods.get(position).getName());
        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClick.OnItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return paymentMethods.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name;

        public ViewHolder(View view) {
            super(view);

            name = view.findViewById(R.id.tv_name);

        }
    }

    public void setOnClick(IOnClickListenerRecyclerView onClick) {
        this.onClick = onClick;
    }
}

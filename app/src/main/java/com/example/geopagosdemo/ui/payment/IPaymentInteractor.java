package com.example.geopagosdemo.ui.payment;

import com.example.geopagosdemo.model.Installments;
import com.example.geopagosdemo.model.Issuer;
import com.example.geopagosdemo.model.PaymentMethod;
import com.example.geopagosdemo.model.preference.PreferenceResponse;
import com.example.geopagosdemo.model.util.PayerCost;

import java.util.ArrayList;

public interface IPaymentInteractor {

    void getPaymentMethods(IGetPMListener listener);
    void getIssuer(String paymentMethodId, IGetIssuerListener listener);
    void getInstallments(String amount, String paymentMethodId, String issuerId, IGetInstallmentsListener listener);
    void createPreference(String amount, PaymentMethod paymentMethod, PayerCost payerCost, IPostPreferenceListener listener);

    interface IGetPMListener {
        void onPMLoaded(ArrayList<PaymentMethod> paymentMethods);
        void showError(int errorCode, String Message);
    }

    interface IGetIssuerListener{
        void onIssuerLoaded(ArrayList<Issuer> issuers);
        void showError(int errorCode, String Message);
    }

    interface IGetInstallmentsListener{
        void onInstallmentsLoaded(ArrayList<Installments> installments);
        void showError(int errorCode, String Message);
    }

    interface IPostPreferenceListener{
        void onCreatedPreference(PreferenceResponse preferenceResponse);
        void showError(int errorCode, String Message);
    }
}

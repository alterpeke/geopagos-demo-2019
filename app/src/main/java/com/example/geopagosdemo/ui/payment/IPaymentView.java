package com.example.geopagosdemo.ui.payment;

import com.example.geopagosdemo.model.Installments;
import com.example.geopagosdemo.model.Issuer;
import com.example.geopagosdemo.model.PaymentMethod;
import com.example.geopagosdemo.model.preference.PreferenceResponse;

import java.util.ArrayList;

public interface IPaymentView {

    void paymentMethodsLoaded(ArrayList<PaymentMethod> paymentMethods);
    void issuersLoaded(ArrayList<Issuer> issuers);
    void installmentsLoaded(ArrayList<Installments> installments);
    void startPayment(PreferenceResponse preferenceResponse);
    void showError(int errorCode, String message);

}

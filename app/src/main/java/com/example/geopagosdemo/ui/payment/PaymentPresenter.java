package com.example.geopagosdemo.ui.payment;

import com.example.geopagosdemo.MainActivity;
import com.example.geopagosdemo.model.Installments;
import com.example.geopagosdemo.model.Issuer;
import com.example.geopagosdemo.model.PaymentMethod;
import com.example.geopagosdemo.model.preference.PreferenceResponse;
import com.example.geopagosdemo.model.util.PayerCost;

import java.util.ArrayList;

import javax.inject.Inject;

public class PaymentPresenter implements IPaymentInteractor.IGetPMListener,
                                         IPaymentInteractor.IGetIssuerListener,
                                         IPaymentInteractor.IGetInstallmentsListener,
                                         IPaymentInteractor.IPostPreferenceListener{

    private IPaymentView view;

    @Inject
    IPaymentInteractor interactor;

    public PaymentPresenter(IPaymentView view) {
        this.view = view;
        MainActivity.getContactsComponents().inject(this);
    }

    public void loadPaymentMethods(){
        interactor.getPaymentMethods(this);
    }

    public void loadIsuers(String paymentMethodId){ interactor.getIssuer(paymentMethodId, this);}

    public void loadInstallments(String amount, String paymentMethodId, String issuerId){
        interactor.getInstallments(amount, paymentMethodId, issuerId, this);
    }

    public void createPreference(String amount, PaymentMethod paymentMethod, PayerCost payerCost){
        interactor.createPreference(amount, paymentMethod, payerCost, this);
    }

    @Override
    public void onPMLoaded(ArrayList<PaymentMethod> paymentMethods) {
        view.paymentMethodsLoaded(paymentMethods);
    }

    @Override
    public void onIssuerLoaded(ArrayList<Issuer> issuers) {
        view.issuersLoaded(issuers);
    }

    @Override
    public void onInstallmentsLoaded(ArrayList<Installments> installments) {
        view.installmentsLoaded(installments);
    }

    @Override
    public void onCreatedPreference(PreferenceResponse preferenceResponse) {
        view.startPayment(preferenceResponse);
    }

    @Override
    public void showError(int errorCode, String Message) {
        view.showError(errorCode, Message);
    }
}

package com.example.geopagosdemo.ui.installments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.geopagosdemo.R;
import com.example.geopagosdemo.model.Installments;
import com.example.geopagosdemo.ui.utils.IOnClickListenerRecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InstallmentsActivity extends AppCompatActivity {

    public final static String INSTALLMENTS = "installments";
    public final static String INSTALLMENT_SELECTED = "installment_selected";

    private ArrayList<Installments> installments;

    @BindView(R.id.rv_forma_pago)
    RecyclerView rvIssuers;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_method);
        ButterKnife.bind(this);

        installments = getIntent().getParcelableArrayListExtra(INSTALLMENTS);

        RecyclerView.LayoutManager layoutManagerSelected_fc = new LinearLayoutManager(this);
        rvIssuers.setLayoutManager(layoutManagerSelected_fc);

        InstallmentsAdapter adapter = new InstallmentsAdapter(this, new ArrayList<>(installments.get(0).getPayerCosts()));
        adapter.setOnClick(new IOnClickListenerRecyclerView() {
            @Override
            public void OnItemClick(int position) {
                Log.e("Selected: ", Integer.toString(position));
                Intent intent = getIntent();
                intent.putExtra(INSTALLMENT_SELECTED, installments.get(0).getPayerCosts().get(position));
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        rvIssuers.setAdapter(adapter);
    }
}

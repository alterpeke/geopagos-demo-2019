package com.example.geopagosdemo.ui.payment;

import com.example.geopagosdemo.MainActivity;
import com.example.geopagosdemo.business.IManager;
import com.example.geopagosdemo.business.InstallmentsManager;
import com.example.geopagosdemo.business.IssuerManager;
import com.example.geopagosdemo.business.PaymentMethodManager;
import com.example.geopagosdemo.business.PreferenceManager;
import com.example.geopagosdemo.model.Installments;
import com.example.geopagosdemo.model.Issuer;
import com.example.geopagosdemo.model.PaymentMethod;
import com.example.geopagosdemo.model.preference.Item;
import com.example.geopagosdemo.model.preference.Payer;
import com.example.geopagosdemo.model.preference.Preference;
import com.example.geopagosdemo.model.preference.PreferencePaymentMethods;
import com.example.geopagosdemo.model.preference.PreferenceResponse;
import com.example.geopagosdemo.model.util.PayerCost;

import java.util.ArrayList;

import javax.inject.Inject;


public class PaymentInteractor implements IPaymentInteractor{

    @Inject
    PaymentMethodManager paymentMethodManager;

    @Inject
    IssuerManager issuerManager;

    @Inject
    InstallmentsManager installmentsManager;

    @Inject
    PreferenceManager preferenceManager;

    public PaymentInteractor() {
        MainActivity.getContactsComponents().inject(this);
    }

    @Override
    public void getPaymentMethods(IGetPMListener listener) {
        paymentMethodManager.load(new IManager.IManagerLoadListener() {
            @Override
            public void onLoaded(ArrayList objectLoaded) {
                listener.onPMLoaded(objectLoaded);
            }

            @Override
            public void onError(int errorCode, String message) {
                listener.showError(errorCode, message);
            }
        });
    }

    @Override
    public void getIssuer(String paymentMethodId, IGetIssuerListener listener) {
        issuerManager.load(paymentMethodId, new IManager.IManagerLoadListener<Issuer>() {
            @Override
            public void onLoaded(ArrayList<Issuer> objectLoaded) {
                listener.onIssuerLoaded(objectLoaded);
            }

            @Override
            public void onError(int errorCode, String message) {
                listener.showError(errorCode, message);
            }
        });
    }

    @Override
    public void getInstallments(String amount, String paymentMethodId, String issuerId, IGetInstallmentsListener listener) {
        installmentsManager.load(amount, paymentMethodId, issuerId, new IManager.IManagerLoadListener<Installments>() {
            @Override
            public void onLoaded(ArrayList<Installments> objectLoaded) {
                listener.onInstallmentsLoaded(objectLoaded);
            }

            @Override
            public void onError(int errorCode, String message) {
                listener.showError(errorCode, message);
            }
        });
    }

    @Override
    public void createPreference(String amount, PaymentMethod paymentMethod, PayerCost payerCost, IPostPreferenceListener listener) {
        Preference preference = new Preference();

        //TODO MOCK
        Item item = new Item();
        item.setTitle("Dummy Item 48");
        item.setDescription("Titulo del Dummy Item 48");
        item.setQuantity(1);
        item.setCurrencyId("ARS");
        item.setUnitPrice(Double.parseDouble(amount));
        ArrayList<Item> items = new ArrayList<>();
        items.add(item);
        preference.setItems(items);

        Payer payer = new Payer();
        payer.setName("John");
        payer.setSurname("Doe");
        payer.setEmail("sarasa@sarasa.com");
        preference.setPayer(payer);

        PreferencePaymentMethods pm = new PreferencePaymentMethods();
        pm.setId(paymentMethod.getId());
        //pm.setInstallments(Integer.parseInt(payerCost.getInstallments().toString()));
        pm.setInstallments(3);
        preference.setPaymentMethods(pm);

        preferenceManager.load(preference, new PreferenceManager.IManagerPreferenceListener<PreferenceResponse>() {
            @Override
            public void onLoaded(PreferenceResponse objectLoaded) {
                listener.onCreatedPreference(objectLoaded);
            }

            @Override
            public void onError(int errorCode, String message) {
                listener.showError(errorCode, message);
            }
        });
    }


}

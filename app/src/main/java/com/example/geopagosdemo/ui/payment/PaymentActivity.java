package com.example.geopagosdemo.ui.payment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.geopagosdemo.R;
import com.example.geopagosdemo.model.Installments;
import com.example.geopagosdemo.model.Issuer;
import com.example.geopagosdemo.model.PaymentMethod;
import com.example.geopagosdemo.model.preference.PreferenceResponse;
import com.example.geopagosdemo.model.util.PayerCost;
import com.example.geopagosdemo.ui.installments.InstallmentsActivity;
import com.example.geopagosdemo.ui.issuer.IssuerActivity;
import com.example.geopagosdemo.ui.payment_method.PaymentMethodActivity;
import com.mercadopago.android.px.core.MercadoPagoCheckout;
import com.mercadopago.android.px.model.Payer;
import com.mercadopago.android.px.model.Payment;
import com.mercadopago.android.px.model.exceptions.MercadoPagoError;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PaymentActivity extends AppCompatActivity implements IPaymentView {

    // --- ATTRIBUTES

    private final static int PAYMENT_METHOD_REQUEST = 1;
    private final static int ISSUER_REQUEST = 2;
    private final static int INSTALLSMENT_REQUEST = 3;
    private static final int MP_REQUEST_CODE = 4;

    private PaymentPresenter presenter;
    private Context context;

    private ArrayList<PaymentMethod> paymentMethods;
    private ArrayList<Issuer> issuers;
    private ArrayList<Installments> installments;

    private PaymentMethod paymentMethodSelected;
    private Issuer issuerSelected;
    private PayerCost installmentSelected;

    @BindView(R.id.et_ingrese_monto)
    EditText monto;

    @BindView(R.id.tv_forma_pago)
    TextView tvFormaPago;

    @BindView(R.id.tv_emisor)
    TextView tvEmisor;

    @BindView(R.id.tv_cuotas)
    TextView tvCuotas;


    // --- ONCREATE

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment);
        ButterKnife.bind(this);
        context = this;

        presenter = new PaymentPresenter(this);
        presenter.loadPaymentMethods();

    }

    // --- LOADING

    @Override
    public void paymentMethodsLoaded(ArrayList<PaymentMethod> paymentMethods) {
        this.paymentMethods = paymentMethods;
        Log.e("DATA LOADED", "OK");
    }

    @Override
    public void issuersLoaded(ArrayList<Issuer> issuers) {
        this.issuers = issuers;
        if(issuers.size()>0){
            Intent intent = new Intent(context, IssuerActivity.class);
            intent.putExtra(IssuerActivity.ISSUERS, issuers);
            startActivityForResult(intent, ISSUER_REQUEST);
        }else{
            //TODO mensaje no existen emisores para ese medio de pago
        }
    }

    @Override
    public void installmentsLoaded(ArrayList<Installments> installments) {
        this.installments = installments;
        if(installments.size()>0) {
            Intent intent = new Intent(context, InstallmentsActivity.class);
            intent.putExtra(InstallmentsActivity.INSTALLMENTS, installments);
            startActivityForResult(intent, INSTALLSMENT_REQUEST);
        }else{
            //TODO mensaje si no existen cuotas
        }
    }

    @Override
    public void startPayment(PreferenceResponse preferenceResponse) {
        //preferenceResponse.setId("340154213-4636d23e-9157-4e36-b748-41fe7a97069a");
        new MercadoPagoCheckout.Builder(context.getString(R.string.publicKey), preferenceResponse.getId()).build()
                .startPayment(this, MP_REQUEST_CODE);
    }

    @Override
    public void showError(int errorCode, String message) {
        Toast.makeText(this, R.string.errorCarga + message, Toast.LENGTH_LONG).show();
        Log.e("DATA LOADED ERROR: ", message);
    }

    // --- EVENTS

    @OnClick(R.id.bt_forma_pago)
    public void getFormaDePago(){
        /*if (monto.getText().toString().isEmpty()){
            Toast.makeText(this, "Debe ingresar el monto primero", Toast.LENGTH_LONG).show();
            return;
        }*/
        Intent intent = new Intent(context, PaymentMethodActivity.class);
        intent.putExtra(PaymentMethodActivity.PAYMENTS, paymentMethods);
        startActivityForResult(intent, PAYMENT_METHOD_REQUEST);
    }

    @OnClick(R.id.bt_emisor)
    public void getBancoEmisor(){
        if (paymentMethodSelected==null){
            Toast.makeText(this, "Debe ingresar el metodo de pago primero", Toast.LENGTH_LONG).show();
            return;
        }
        presenter.loadIsuers(paymentMethodSelected.getId());
    }

    @OnClick(R.id.bt_cuotas)
    public void getCuotas(){
        if (monto.getText().toString().isEmpty() || paymentMethodSelected==null || issuerSelected==null){
            Toast.makeText(this, "Debe ingresar los datos anteriores (Monto, Metodo de Pago y Banco)", Toast.LENGTH_LONG).show();
            return;
        }
        presenter.loadInstallments(monto.getText().toString(), paymentMethodSelected.getId(), issuerSelected.getId());
    }

    @OnClick(R.id.bt_pagar)
    public void pagar(){
        if (monto.getText().toString().isEmpty() || paymentMethodSelected==null || installmentSelected==null) {
            Toast.makeText(this, "Debe ingresar los datos anteriores (Monto, Metodo de Pago y Banco)", Toast.LENGTH_LONG).show();
            return;
        }
        presenter.createPreference(monto.getText().toString(), paymentMethodSelected, installmentSelected);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        // Check which request we're responding to
        if (requestCode == PAYMENT_METHOD_REQUEST) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                paymentMethodSelected = data.getParcelableExtra(PaymentMethodActivity.PAYMENT_SELECTED);
                tvFormaPago.setText(paymentMethodSelected.getName());
            }
        }
        if(requestCode == ISSUER_REQUEST){
            if (resultCode == RESULT_OK){
                issuerSelected = data.getParcelableExtra(IssuerActivity.ISSUER_SELECTED);
                tvEmisor.setText(issuerSelected.getName());
            }
        }
        if(requestCode == INSTALLSMENT_REQUEST){
            if (resultCode == RESULT_OK){
                installmentSelected = data.getParcelableExtra(InstallmentsActivity.INSTALLMENT_SELECTED);
                tvCuotas.setText(installmentSelected.getRecommendedMessage());
            }
        }
        if(requestCode == MP_REQUEST_CODE){
            if (resultCode == MercadoPagoCheckout.PAYMENT_RESULT_CODE) {
                final Payment payment = (Payment) data.getSerializableExtra(MercadoPagoCheckout.EXTRA_PAYMENT_RESULT);
                Toast.makeText(this, "Resultado del Pago: "+payment.getPaymentStatus(), Toast.LENGTH_LONG).show();
            }else if (resultCode == RESULT_CANCELED){
                if (data != null && data.getExtras() != null
                        && data.getExtras().containsKey(MercadoPagoCheckout.EXTRA_ERROR)) {
                    final MercadoPagoError mercadoPagoError =
                            (MercadoPagoError) data.getSerializableExtra(MercadoPagoCheckout.EXTRA_ERROR);
                    Toast.makeText(this, "Error "+mercadoPagoError.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }
    }
}

package com.example.geopagosdemo.ui.installments;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.geopagosdemo.R;
import com.example.geopagosdemo.model.util.PayerCost;
import com.example.geopagosdemo.ui.utils.IOnClickListenerRecyclerView;

import java.util.ArrayList;

public class InstallmentsAdapter extends RecyclerView.Adapter<InstallmentsAdapter.ViewHolder> {

    private ArrayList<PayerCost> installments;
    private Context context;
    private IOnClickListenerRecyclerView onClick;

    public InstallmentsAdapter(Context context, ArrayList<PayerCost> installments) {
        this.context = context;
        this.installments = installments;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_installment, parent, false);
        return new InstallmentsAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.installmentName.setText(installments.get(position).getRecommendedMessage());
        holder.installmentName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClick.OnItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return installments.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView installmentName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            installmentName = itemView.findViewById(R.id.tv_nameInstallment);
        }
    }

    public void setOnClick(IOnClickListenerRecyclerView onClick) {
        this.onClick = onClick;
    }
}

package com.example.geopagosdemo.business;

import com.example.geopagosdemo.MainActivity;
import com.example.geopagosdemo.model.Issuer;
import com.example.geopagosdemo.service.APIServices.IssuerService;
import com.example.geopagosdemo.service.APIServices.interfaces.IIssuerService;

import java.util.ArrayList;

import javax.inject.Inject;

public class IssuerManager {

    @Inject
    IssuerService issuerService;

    public IssuerManager() {
        MainActivity.getContactsComponents().inject(this);
    }

    public ArrayList<Issuer> load(String paymentMethodId, IManager.IManagerLoadListener listener) {

        issuerService.doGetRequest(paymentMethodId, new IIssuerService.IResponseListener<Issuer>() {
            @Override
            public void onResponseSuccess(ArrayList<Issuer> objects) {
                listener.onLoaded(objects);
            }

            @Override
            public void onResponseError(int errorCode, String message) {
                listener.onError(errorCode, message);
            }
        });

        return null;
    }
}

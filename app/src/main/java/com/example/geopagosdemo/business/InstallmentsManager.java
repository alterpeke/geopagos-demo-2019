package com.example.geopagosdemo.business;

import com.example.geopagosdemo.MainActivity;
import com.example.geopagosdemo.model.Installments;
import com.example.geopagosdemo.service.APIServices.InstallmentsService;
import com.example.geopagosdemo.service.APIServices.interfaces.IInstallmentsService;

import java.util.ArrayList;

import javax.inject.Inject;

public class InstallmentsManager {

    @Inject
    InstallmentsService installmentsService;

    public InstallmentsManager() {
        MainActivity.getContactsComponents().inject(this);
    }

    public ArrayList<Installments> load(String amount,
                                        String paymentMethodId,
                                        String issuerId,
                                        IManager.IManagerLoadListener<Installments> listener) {
        installmentsService.doGetRequest(amount, paymentMethodId, issuerId, new IInstallmentsService.IResponseListener<Installments>() {
            @Override
            public void onResponseSuccess(ArrayList<Installments> objects) {
                listener.onLoaded(objects);
            }

            @Override
            public void onResponseError(int errorCode, String message) {
                listener.onError(errorCode, message);
            }
        });
        return null;
    }
}

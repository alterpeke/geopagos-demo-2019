package com.example.geopagosdemo.business;

import java.util.ArrayList;

public interface IManager<T> {

    ArrayList<T> load(IManagerLoadListener listener);

    interface IManagerLoadListener<T>{
        void onLoaded(ArrayList<T> objectLoaded);
        void onError(int errorCode, String message);
    }

}

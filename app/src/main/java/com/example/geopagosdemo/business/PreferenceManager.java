package com.example.geopagosdemo.business;

import com.example.geopagosdemo.MainActivity;
import com.example.geopagosdemo.model.preference.Preference;
import com.example.geopagosdemo.model.preference.PreferenceResponse;
import com.example.geopagosdemo.service.APIServices.PreferenceService;
import com.example.geopagosdemo.service.APIServices.interfaces.IPreferenceService;

import java.util.ArrayList;

import javax.inject.Inject;

public class PreferenceManager{

    @Inject
    PreferenceService preferenceService;

    public PreferenceManager() {
        MainActivity.getContactsComponents().inject(this);
    }

    public ArrayList<Preference> load(Preference preference, IManagerPreferenceListener<PreferenceResponse> listener) {

        preferenceService.doPostRequest(preference, new IPreferenceService.IResponseListener<PreferenceResponse>() {
            @Override
            public void onResponseSuccess(PreferenceResponse object) {
                listener.onLoaded(object);
            }

            @Override
            public void onResponseError(int errorCode, String message) {
                listener.onError(errorCode, message);
            }
        });
        return null;
    }

    public interface IManagerPreferenceListener<T>{
        void onLoaded(T objectLoaded);
        void onError(int errorCode, String message);
    }
}

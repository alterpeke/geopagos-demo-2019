package com.example.geopagosdemo.business;

import com.example.geopagosdemo.MainActivity;
import com.example.geopagosdemo.model.PaymentMethod;
import com.example.geopagosdemo.service.APIServices.PaymentMethodService;
import com.example.geopagosdemo.service.APIServices.interfaces.IPaymentMethodService;

import java.util.ArrayList;

import javax.inject.Inject;

public class PaymentMethodManager implements IManager<PaymentMethod> {

    @Inject
    PaymentMethodService paymentMethodService;

    public PaymentMethodManager() {
        MainActivity.getContactsComponents().inject(this);
    }

    @Override
    public ArrayList<PaymentMethod> load(IManagerLoadListener listener) {

        paymentMethodService.doGetRequest(new IPaymentMethodService.IResponseListener<PaymentMethod>() {
            @Override
            public void onResponseSuccess(ArrayList<PaymentMethod> objects) {
                listener.onLoaded(objects);
            }

            @Override
            public void onResponseError(int errorCode, String message) {
                listener.onError(errorCode, message);
            }
        });
        return null;
    }
}

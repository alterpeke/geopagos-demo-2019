package com.example.geopagosdemo.service;

import android.content.Context;

import com.example.geopagosdemo.R;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitInstance {

    private Retrofit retrofit;
    private OkHttpClient.Builder httpClient;
    private Retrofit.Builder builder;

    public RetrofitInstance(Context context){
        builder = new Retrofit.Builder()
                .baseUrl(context.getString(R.string.endpoint))
                .addConverterFactory(GsonConverterFactory.create());


        httpClient = new OkHttpClient.Builder();
    }

    public <S> S createService(Class<S> serviceClass) {
        AuthenticationInterceptor interceptor = new AuthenticationInterceptor();

        if (!httpClient.interceptors().contains(interceptor)) {
            httpClient.addInterceptor(interceptor);

            builder.client(httpClient.build());
            retrofit = builder.build();
        }


        return retrofit.create(serviceClass);
    }


    public class AuthenticationInterceptor implements Interceptor {

        @Override
        public okhttp3.Response intercept(Chain chain) throws IOException {

            Request original = chain.request();

            Request.Builder builder = original.newBuilder();

            builder.header("Accept", "application/json");
            builder.header("Content-type", "Application/Json");

            Request request = builder.build();

            return chain.proceed(request);
        }
    }

}

package com.example.geopagosdemo.service.APIServices;

import android.content.Context;
import android.util.Log;

import com.example.geopagosdemo.MainActivity;
import com.example.geopagosdemo.R;
import com.example.geopagosdemo.model.PaymentMethod;
import com.example.geopagosdemo.service.IRetrofitInstance;
import com.example.geopagosdemo.service.RetrofitInstance;
import com.example.geopagosdemo.service.APIServices.interfaces.IPaymentMethodService;

import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentMethodService implements IPaymentMethodService<PaymentMethod> {

    @Inject
    RetrofitInstance retrofitInstance;

    @Inject
    Context context;

    public PaymentMethodService() {
        MainActivity.getContactsComponents().inject(this);
    }

    @Override
    public void doGetRequest(IResponseListener<PaymentMethod> listener) {

        IRetrofitInstance service = retrofitInstance.createService(IRetrofitInstance.class);

        Call<ArrayList<PaymentMethod>> call = service.getPaymentMethods(context.getString(R.string.accessToken));

        Log.i("URL: ", call.request().url().toString());

        call.enqueue(new Callback<ArrayList<PaymentMethod>>() {
            @Override
            public void onResponse(Call<ArrayList<PaymentMethod>> call, Response<ArrayList<PaymentMethod>> response) {
                if (response.isSuccessful()) {
                    listener.onResponseSuccess(response.body());
                } else {
                    listener.onResponseError(response.code(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<PaymentMethod>> call, Throwable t) {
                listener.onResponseError(-1, t.getMessage());
            }
        });

    }
}

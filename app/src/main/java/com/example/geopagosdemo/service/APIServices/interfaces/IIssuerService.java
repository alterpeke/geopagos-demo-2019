package com.example.geopagosdemo.service.APIServices.interfaces;

import java.util.ArrayList;

public interface IIssuerService<T> {

    void doGetRequest(String paymentMethodId, IResponseListener<T> listener);

    interface IResponseListener<T> {
        void onResponseSuccess(ArrayList<T> objects);
        void onResponseError(int errorCode, String message);
    }

}

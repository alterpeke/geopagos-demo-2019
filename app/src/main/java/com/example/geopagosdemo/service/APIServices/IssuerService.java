package com.example.geopagosdemo.service.APIServices;

import android.content.Context;
import android.util.Log;

import com.example.geopagosdemo.MainActivity;
import com.example.geopagosdemo.R;
import com.example.geopagosdemo.model.Issuer;
import com.example.geopagosdemo.service.IRetrofitInstance;
import com.example.geopagosdemo.service.RetrofitInstance;
import com.example.geopagosdemo.service.APIServices.interfaces.IIssuerService;

import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IssuerService implements IIssuerService<Issuer> {

    @Inject
    RetrofitInstance retrofitInstance;

    @Inject
    Context context;

    public IssuerService() {
        MainActivity.getContactsComponents().inject(this);
    }

    @Override
    public void doGetRequest(String paymentMethodId, IResponseListener<Issuer> listener) {

        IRetrofitInstance service = retrofitInstance.createService(IRetrofitInstance.class);

        Call<ArrayList<Issuer>> call = service.getIssuers(context.getString(R.string.publicKey),
                                                          paymentMethodId);

        Log.i("URL: ", call.request().url().toString());

        call.enqueue(new Callback<ArrayList<Issuer>>() {
            @Override
            public void onResponse(Call<ArrayList<Issuer>> call, Response<ArrayList<Issuer>> response) {
                if (response.isSuccessful()) {
                    listener.onResponseSuccess(response.body());
                } else {
                    listener.onResponseError(response.code(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Issuer>> call, Throwable t) {
                listener.onResponseError(-1, t.getMessage());
            }
        });
    }
}

package com.example.geopagosdemo.service.APIServices;

import android.content.Context;
import android.util.Log;

import com.example.geopagosdemo.MainActivity;
import com.example.geopagosdemo.R;
import com.example.geopagosdemo.model.preference.Preference;
import com.example.geopagosdemo.model.preference.PreferenceResponse;
import com.example.geopagosdemo.service.IRetrofitInstance;
import com.example.geopagosdemo.service.RetrofitInstance;
import com.example.geopagosdemo.service.APIServices.interfaces.IPreferenceService;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PreferenceService implements IPreferenceService<PreferenceResponse> {

    @Inject
    RetrofitInstance retrofitInstance;

    @Inject
    Context context;

    public PreferenceService() {
        MainActivity.getContactsComponents().inject(this);
    }

    @Override
    public void doPostRequest(Preference preference, IResponseListener<PreferenceResponse> listener) {

        IRetrofitInstance service = retrofitInstance.createService(IRetrofitInstance.class);

        Call<PreferenceResponse> call = service.postPreference(context.getString(R.string.accessToken), preference);

        Log.i("URL: ", call.request().url().toString());

        call.enqueue(new Callback<PreferenceResponse>() {
            @Override
            public void onResponse(Call<PreferenceResponse> call, Response<PreferenceResponse> response) {
                if (response.isSuccessful()) {
                    listener.onResponseSuccess(response.body());
                } else {
                    listener.onResponseError(response.code(), response.message());
                }
            }

            @Override
            public void onFailure(Call<PreferenceResponse> call, Throwable t) {
                listener.onResponseError(-1, t.getMessage());
            }
        });
    }
}

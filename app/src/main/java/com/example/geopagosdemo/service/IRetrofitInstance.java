package com.example.geopagosdemo.service;

import com.example.geopagosdemo.model.Installments;
import com.example.geopagosdemo.model.Issuer;
import com.example.geopagosdemo.model.PaymentMethod;
import com.example.geopagosdemo.model.preference.Preference;
import com.example.geopagosdemo.model.preference.PreferenceResponse;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface IRetrofitInstance {

    // Payment Method
    @GET("v1/payment_methods")
    Call<ArrayList<PaymentMethod>> getPaymentMethods(@Query("access_token") String token);

    //Issuers
    @GET("v1/payment_methods/card_issuers")
    Call<ArrayList<Issuer>> getIssuers(@Query("public_key") String publicKey,
                                       @Query("payment_method_id") String paymentMethodId);

    //Instalments
    @GET("v1/payment_methods/installments")
    Call<ArrayList<Installments>> getInstallments(@Query("public_key") String publicKey,
                                                  @Query("amount") String amount,
                                                  @Query("payment_method_id") String paymentMethodId,
                                                  @Query("issuer.id") String issuerId);

    //Preference
    @POST("checkout/preferences")
    Call<PreferenceResponse> postPreference(@Query("access_token") String token,
                                            @Body Preference preference);

}

package com.example.geopagosdemo.service.APIServices.interfaces;

import com.example.geopagosdemo.model.preference.Preference;

import java.util.ArrayList;

public interface IPreferenceService<T> {

    void doPostRequest(Preference preference, IResponseListener<T> listener);

    interface IResponseListener<T> {
        void onResponseSuccess(T object);
        void onResponseError(int errorCode, String message);
    }

}

package com.example.geopagosdemo.service.APIServices.interfaces;

import java.util.ArrayList;

public interface IPaymentMethodService<T> {

    void doGetRequest(IResponseListener<T> listener);

    interface IResponseListener<T> {
        void onResponseSuccess(ArrayList<T> objects);
        void onResponseError(int errorCode, String message);
    }

}

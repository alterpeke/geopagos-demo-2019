package com.example.geopagosdemo.service.APIServices;

import android.content.Context;
import android.util.Log;

import com.example.geopagosdemo.MainActivity;
import com.example.geopagosdemo.R;
import com.example.geopagosdemo.model.Installments;
import com.example.geopagosdemo.service.IRetrofitInstance;
import com.example.geopagosdemo.service.RetrofitInstance;
import com.example.geopagosdemo.service.APIServices.interfaces.IInstallmentsService;

import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InstallmentsService implements IInstallmentsService<Installments> {


    @Inject
    RetrofitInstance retrofitInstance;

    @Inject
    Context context;

    public InstallmentsService() {
        MainActivity.getContactsComponents().inject(this);
    }

    @Override
    public void doGetRequest(String amount, String paymentMethodId, String issuerId, IResponseListener<Installments> listener) {

        IRetrofitInstance service = retrofitInstance.createService(IRetrofitInstance.class);

        Call<ArrayList<Installments>> call = service.getInstallments(context.getString(R.string.publicKey),
                                                                     amount,
                                                                     paymentMethodId,
                                                                     issuerId);

        Log.i("URL: ", call.request().url().toString());

        call.enqueue(new Callback<ArrayList<Installments>>() {
            @Override
            public void onResponse(Call<ArrayList<Installments>> call, Response<ArrayList<Installments>> response) {
                if (response.isSuccessful()) {
                    listener.onResponseSuccess(response.body());
                } else {
                    listener.onResponseError(response.code(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Installments>> call, Throwable t) {
                listener.onResponseError(-1, t.getMessage());
            }
        });
    }
}

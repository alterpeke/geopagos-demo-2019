package com.example.geopagosdemo.service.APIServices.interfaces;

import java.util.ArrayList;

public interface IInstallmentsService<T> {

    void doGetRequest(String amount, String paymentMethodId, String issuerId, IResponseListener<T> listener);

    interface IResponseListener<T> {
        void onResponseSuccess(ArrayList<T> objects);
        void onResponseError(int errorCode, String message);
    }
}

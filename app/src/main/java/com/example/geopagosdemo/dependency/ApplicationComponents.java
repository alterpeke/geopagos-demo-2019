package com.example.geopagosdemo.dependency;

import com.example.geopagosdemo.MainActivity;
import com.example.geopagosdemo.business.InstallmentsManager;
import com.example.geopagosdemo.business.IssuerManager;
import com.example.geopagosdemo.business.PaymentMethodManager;
import com.example.geopagosdemo.business.PreferenceManager;
import com.example.geopagosdemo.service.APIServices.InstallmentsService;
import com.example.geopagosdemo.service.APIServices.IssuerService;
import com.example.geopagosdemo.service.APIServices.PaymentMethodService;
import com.example.geopagosdemo.service.APIServices.PreferenceService;
import com.example.geopagosdemo.ui.payment.PaymentInteractor;
import com.example.geopagosdemo.ui.payment.PaymentPresenter;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class,
        BusinessModule.class,
        ServiceModule.class})
public interface ApplicationComponents {

    void inject(MainActivity application);
    void inject(PaymentMethodManager paymentMethodManager);
    void inject(InstallmentsManager installmentsManager);
    void inject(IssuerManager issuerManager);
    void inject(PreferenceManager preferenceManager);

    void inject(PaymentMethodService paymentMethodService);
    void inject(IssuerService issuerService);
    void inject(InstallmentsService installmentsService);
    void inject(PreferenceService preferenceService);

    void inject(PaymentPresenter paymentPresenter);
    void inject(PaymentInteractor paymentInteractor);

}

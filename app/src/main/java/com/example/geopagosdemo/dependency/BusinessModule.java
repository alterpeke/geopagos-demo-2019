package com.example.geopagosdemo.dependency;

import com.example.geopagosdemo.business.InstallmentsManager;
import com.example.geopagosdemo.business.IssuerManager;
import com.example.geopagosdemo.business.PaymentMethodManager;
import com.example.geopagosdemo.business.PreferenceManager;
import com.example.geopagosdemo.ui.payment.IPaymentInteractor;
import com.example.geopagosdemo.ui.payment.PaymentInteractor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class BusinessModule {

    @Singleton
    @Provides
    public PaymentMethodManager providesPaymentMethodManager(){return new PaymentMethodManager();}

    @Singleton
    @Provides
    public IssuerManager providesIssuerManager(){return new IssuerManager();}

    @Singleton
    @Provides
    public InstallmentsManager providesInstallmentsManager(){return new InstallmentsManager();}

    @Singleton
    @Provides
    public PreferenceManager providesPreferenceManager(){return new PreferenceManager();}

    @Singleton
    @Provides
    public IPaymentInteractor providesPaymentInteractor(){return new PaymentInteractor();}

}

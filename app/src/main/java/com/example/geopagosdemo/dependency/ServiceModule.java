package com.example.geopagosdemo.dependency;

import android.content.Context;

import com.example.geopagosdemo.service.APIServices.InstallmentsService;
import com.example.geopagosdemo.service.APIServices.IssuerService;
import com.example.geopagosdemo.service.APIServices.PaymentMethodService;
import com.example.geopagosdemo.service.RetrofitInstance;
import com.example.geopagosdemo.service.APIServices.PreferenceService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ServiceModule {

    @Singleton
    @Provides
    public RetrofitInstance providesRetrofit(Context context) {
        return new RetrofitInstance(context);
    }

    @Singleton
    @Provides
    public PaymentMethodService providesPaymentMethodService(){return new PaymentMethodService();}

    @Singleton
    @Provides
    public IssuerService providesIssuerService(){return new IssuerService();}

    @Singleton
    @Provides
    public InstallmentsService providesInstallmentsService(){return new InstallmentsService();}

    @Singleton
    @Provides
    public PreferenceService providesPreferenceService(){return new PreferenceService();}

}

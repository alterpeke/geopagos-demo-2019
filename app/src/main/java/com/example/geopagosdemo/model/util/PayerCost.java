package com.example.geopagosdemo.model.util;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class PayerCost implements Parcelable {

    @SerializedName("installments")
    private Long installments;

    @SerializedName("installment_rate")
    private Float installmentRate;

    @SerializedName("discount_rate")
    private Long discountRate;

    @SerializedName("reimbursement_rate")
    private Object reimbursementRate;

    @SerializedName("labels")
    private List<String> labels = null;

    @SerializedName("installment_rate_collector")
    private List<String> installmentRateCollector = null;

    @SerializedName("min_allowed_amount")
    private Long minAllowedAmount;

    @SerializedName("max_allowed_amount")
    private Long maxAllowedAmount;

    @SerializedName("recommended_message")
    private String recommendedMessage;

    @SerializedName("installment_amount")
    private Float installmentAmount;

    @SerializedName("total_amount")
    private Float totalAmount;

    public Long getInstallments() {
        return installments;
    }

    public void setInstallments(Long installments) {
        this.installments = installments;
    }

    public Float getInstallmentRate() {
        return installmentRate;
    }

    public void setInstallmentRate(Float installmentRate) {
        this.installmentRate = installmentRate;
    }

    public Long getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(Long discountRate) {
        this.discountRate = discountRate;
    }

    public Object getReimbursementRate() {
        return reimbursementRate;
    }

    public void setReimbursementRate(Object reimbursementRate) {
        this.reimbursementRate = reimbursementRate;
    }

    public List<String> getLabels() {
        return labels;
    }

    public void setLabels(List<String> labels) {
        this.labels = labels;
    }

    public List<String> getInstallmentRateCollector() {
        return installmentRateCollector;
    }

    public void setInstallmentRateCollector(List<String> installmentRateCollector) {
        this.installmentRateCollector = installmentRateCollector;
    }

    public Long getMinAllowedAmount() {
        return minAllowedAmount;
    }

    public void setMinAllowedAmount(Long minAllowedAmount) {
        this.minAllowedAmount = minAllowedAmount;
    }

    public Long getMaxAllowedAmount() {
        return maxAllowedAmount;
    }

    public void setMaxAllowedAmount(Long maxAllowedAmount) {
        this.maxAllowedAmount = maxAllowedAmount;
    }

    public String getRecommendedMessage() {
        return recommendedMessage;
    }

    public void setRecommendedMessage(String recommendedMessage) {
        this.recommendedMessage = recommendedMessage;
    }

    public Float getInstallmentAmount() {
        return installmentAmount;
    }

    public void setInstallmentAmount(Float installmentAmount) {
        this.installmentAmount = installmentAmount;
    }

    public Float getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Float totalAmount) {
        this.totalAmount = totalAmount;
    }

    protected PayerCost(Parcel in) {
        installments = in.readByte() == 0x00 ? null : in.readLong();
        installmentRate = in.readByte() == 0x00 ? null : in.readFloat();
        discountRate = in.readByte() == 0x00 ? null : in.readLong();
        reimbursementRate = (Object) in.readValue(Object.class.getClassLoader());
        if (in.readByte() == 0x01) {
            labels = new ArrayList<String>();
            in.readList(labels, String.class.getClassLoader());
        } else {
            labels = null;
        }
        if (in.readByte() == 0x01) {
            installmentRateCollector = new ArrayList<String>();
            in.readList(installmentRateCollector, String.class.getClassLoader());
        } else {
            installmentRateCollector = null;
        }
        minAllowedAmount = in.readByte() == 0x00 ? null : in.readLong();
        maxAllowedAmount = in.readByte() == 0x00 ? null : in.readLong();
        recommendedMessage = in.readString();
        installmentAmount = in.readByte() == 0x00 ? null : in.readFloat();
        totalAmount = in.readByte() == 0x00 ? null : in.readFloat();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (installments == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeLong(installments);
        }
        if (installmentRate == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeFloat(installmentRate);
        }
        if (discountRate == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeLong(discountRate);
        }
        dest.writeValue(reimbursementRate);
        if (labels == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(labels);
        }
        if (installmentRateCollector == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(installmentRateCollector);
        }
        if (minAllowedAmount == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeLong(minAllowedAmount);
        }
        if (maxAllowedAmount == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeLong(maxAllowedAmount);
        }
        dest.writeString(recommendedMessage);
        if (installmentAmount == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeFloat(installmentAmount);
        }
        if (totalAmount == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeFloat(totalAmount);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<PayerCost> CREATOR = new Parcelable.Creator<PayerCost>() {
        @Override
        public PayerCost createFromParcel(Parcel in) {
            return new PayerCost(in);
        }

        @Override
        public PayerCost[] newArray(int size) {
            return new PayerCost[size];
        }
    };
    
}

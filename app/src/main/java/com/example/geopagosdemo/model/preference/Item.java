package com.example.geopagosdemo.model.preference;

import com.google.gson.annotations.SerializedName;

public class Item {

    @SerializedName("title")
    private String title;

    @SerializedName("description")
    private String description;

    @SerializedName("quantity")
    private Integer quantity;

    @SerializedName("currency_id")
    private String currencyId;

    @SerializedName("unit_price")
    private Double unitPrice;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
    }

    public Double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }
    
}

package com.example.geopagosdemo.model.preference;

import com.google.gson.annotations.SerializedName;
import com.mercadopago.android.px.model.PaymentMethods;

import java.util.List;

public class Preference {

    @SerializedName("items")
    private List<Item> items = null;

    @SerializedName("payer")
    private Payer payer;

    @SerializedName("payment_methods")
    private PreferencePaymentMethods paymentMethods;

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public Payer getPayer() {
        return payer;
    }

    public void setPayer(Payer payer) {
        this.payer = payer;
    }

    public PreferencePaymentMethods getPaymentMethods() {
        return paymentMethods;
    }

    public void setPaymentMethods(PreferencePaymentMethods paymentMethods) {
        this.paymentMethods = paymentMethods;
    }

}

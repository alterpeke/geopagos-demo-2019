package com.example.geopagosdemo.model.util;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class SecurityCode implements Parcelable {

    @SerializedName("length")
    private Long length;

    @SerializedName("card_location")
    private String cardLocation;

    @SerializedName("mode")
    private String mode;

    public Long getLength() {
        return length;
    }

    public void setLength(Long length) {
        this.length = length;
    }

    public String getCardLocation() {
        return cardLocation;
    }

    public void setCardLocation(String cardLocation) {
        this.cardLocation = cardLocation;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    protected SecurityCode(Parcel in) {
        length = in.readByte() == 0x00 ? null : in.readLong();
        cardLocation = in.readString();
        mode = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (length == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeLong(length);
        }
        dest.writeString(cardLocation);
        dest.writeString(mode);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<SecurityCode> CREATOR = new Parcelable.Creator<SecurityCode>() {
        @Override
        public SecurityCode createFromParcel(Parcel in) {
            return new SecurityCode(in);
        }

        @Override
        public SecurityCode[] newArray(int size) {
            return new SecurityCode[size];
        }
    };
}

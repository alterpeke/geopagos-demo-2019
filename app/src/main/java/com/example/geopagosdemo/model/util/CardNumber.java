package com.example.geopagosdemo.model.util;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class CardNumber implements Parcelable {

    @SerializedName("validation")
    private String validation;

    @SerializedName("length")
    private Long length;

    public String getValidation() {
        return validation;
    }

    public void setValidation(String validation) {
        this.validation = validation;
    }

    public Long getLength() {
        return length;
    }

    public void setLength(Long length) {
        this.length = length;
    }

    protected CardNumber(Parcel in) {
        validation = in.readString();
        length = in.readByte() == 0x00 ? null : in.readLong();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(validation);
        if (length == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeLong(length);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<CardNumber> CREATOR = new Parcelable.Creator<CardNumber>() {
        @Override
        public CardNumber createFromParcel(Parcel in) {
            return new CardNumber(in);
        }

        @Override
        public CardNumber[] newArray(int size) {
            return new CardNumber[size];
        }
    };
}

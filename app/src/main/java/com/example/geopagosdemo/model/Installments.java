package com.example.geopagosdemo.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.geopagosdemo.model.util.PayerCost;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Installments implements Parcelable {

    @SerializedName("payment_method_id")
    private String paymentMethodId;

    @SerializedName("payment_type_id")
    private String paymentTypeId;

    @SerializedName("issuer")
    private Issuer issuer;

    @SerializedName("processing_mode")
    private String processingMode;

    @SerializedName("merchant_account_id")
    private Object merchantAccountId;

    @SerializedName("payer_costs")
    private List<PayerCost> payerCosts = null;

    @SerializedName("agreements")
    private Object agreements;

    public String getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(String paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public String getPaymentTypeId() {
        return paymentTypeId;
    }

    public void setPaymentTypeId(String paymentTypeId) {
        this.paymentTypeId = paymentTypeId;
    }

    public Issuer getIssuer() {
        return issuer;
    }

    public void setIssuer(Issuer issuer) {
        this.issuer = issuer;
    }

    public String getProcessingMode() {
        return processingMode;
    }

    public void setProcessingMode(String processingMode) {
        this.processingMode = processingMode;
    }

    public Object getMerchantAccountId() {
        return merchantAccountId;
    }

    public void setMerchantAccountId(Object merchantAccountId) {
        this.merchantAccountId = merchantAccountId;
    }

    public List<PayerCost> getPayerCosts() {
        return payerCosts;
    }

    public void setPayerCosts(List<PayerCost> payerCosts) {
        this.payerCosts = payerCosts;
    }

    public Object getAgreements() {
        return agreements;
    }

    public void setAgreements(Object agreements) {
        this.agreements = agreements;
    }

    protected Installments(Parcel in) {
        paymentMethodId = in.readString();
        paymentTypeId = in.readString();
        issuer = (Issuer) in.readValue(Issuer.class.getClassLoader());
        processingMode = in.readString();
        merchantAccountId = (Object) in.readValue(Object.class.getClassLoader());
        if (in.readByte() == 0x01) {
            payerCosts = new ArrayList<PayerCost>();
            in.readList(payerCosts, PayerCost.class.getClassLoader());
        } else {
            payerCosts = null;
        }
        agreements = (Object) in.readValue(Object.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(paymentMethodId);
        dest.writeString(paymentTypeId);
        dest.writeValue(issuer);
        dest.writeString(processingMode);
        dest.writeValue(merchantAccountId);
        if (payerCosts == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(payerCosts);
        }
        dest.writeValue(agreements);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Installments> CREATOR = new Parcelable.Creator<Installments>() {
        @Override
        public Installments createFromParcel(Parcel in) {
            return new Installments(in);
        }

        @Override
        public Installments[] newArray(int size) {
            return new Installments[size];
        }
    };
}

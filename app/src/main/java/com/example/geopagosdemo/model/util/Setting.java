package com.example.geopagosdemo.model.util;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Setting implements Parcelable {

    @SerializedName("card_number")
    private CardNumber cardNumber;

    @SerializedName("bin")
    private Bin bin;

    @SerializedName("security_code")
    private SecurityCode securityCode;

    public CardNumber getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(CardNumber cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Bin getBin() {
        return bin;
    }

    public void setBin(Bin bin) {
        this.bin = bin;
    }

    public SecurityCode getSecurityCode() {
        return securityCode;
    }

    public void setSecurityCode(SecurityCode securityCode) {
        this.securityCode = securityCode;
    }

    protected Setting(Parcel in) {
        cardNumber = (CardNumber) in.readValue(CardNumber.class.getClassLoader());
        bin = (Bin) in.readValue(Bin.class.getClassLoader());
        securityCode = (SecurityCode) in.readValue(SecurityCode.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(cardNumber);
        dest.writeValue(bin);
        dest.writeValue(securityCode);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Setting> CREATOR = new Parcelable.Creator<Setting>() {
        @Override
        public Setting createFromParcel(Parcel in) {
            return new Setting(in);
        }

        @Override
        public Setting[] newArray(int size) {
            return new Setting[size];
        }
    };
}

package com.example.geopagosdemo.model.preference;

import com.google.gson.annotations.SerializedName;

public class PreferencePaymentMethods {

    @SerializedName("id")
    private String id;

    @SerializedName("installments")
    private Integer installments;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getInstallments() {
        return installments;
    }

    public void setInstallments(Integer installments) {
        this.installments = installments;
    }
}

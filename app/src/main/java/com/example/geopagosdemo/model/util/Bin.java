package com.example.geopagosdemo.model.util;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Bin implements Parcelable {

    @SerializedName("pattern")
    private String pattern;

    @SerializedName("installments_pattern")
    private String installmentsPattern;

    @SerializedName("exclusion_pattern")
    private Object exclusionPattern;

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public String getInstallmentsPattern() {
        return installmentsPattern;
    }

    public void setInstallmentsPattern(String installmentsPattern) {
        this.installmentsPattern = installmentsPattern;
    }

    public Object getExclusionPattern() {
        return exclusionPattern;
    }

    public void setExclusionPattern(Object exclusionPattern) {
        this.exclusionPattern = exclusionPattern;
    }

    protected Bin(Parcel in) {
        pattern = in.readString();
        installmentsPattern = in.readString();
        exclusionPattern = (Object) in.readValue(Object.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(pattern);
        dest.writeString(installmentsPattern);
        dest.writeValue(exclusionPattern);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Bin> CREATOR = new Parcelable.Creator<Bin>() {
        @Override
        public Bin createFromParcel(Parcel in) {
            return new Bin(in);
        }

        @Override
        public Bin[] newArray(int size) {
            return new Bin[size];
        }
    };
}

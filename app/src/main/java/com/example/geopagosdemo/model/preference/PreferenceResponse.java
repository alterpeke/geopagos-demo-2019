package com.example.geopagosdemo.model.preference;

import com.google.gson.annotations.SerializedName;

public class PreferenceResponse {

    @SerializedName("id")
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

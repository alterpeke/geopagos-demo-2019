package com.example.geopagosdemo.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.geopagosdemo.model.util.Setting;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class PaymentMethod implements Parcelable {

    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("payment_type_id")
    private String paymentTypeId;

    @SerializedName("status")
    private String status;

    @SerializedName("secure_thumbnail")
    private String secureThumbnail;

    @SerializedName("thumbnail")
    private String thumbnail;

    @SerializedName("deferred_capture")
    private String deferredCapture;

    @SerializedName("settings")
    private List<Setting> settings = null;

    @SerializedName("additional_info_needed")
    private List<String> additionalInfoNeeded = null;

    @SerializedName("min_allowed_amount")
    private Double minAllowedAmount;

    @SerializedName("max_allowed_amount")
    private Long maxAllowedAmount;

    @SerializedName("accreditation_time")
    private Long accreditationTime;

    @SerializedName("financial_institutions")
    private List<Object> financialInstitutions = null;

    @SerializedName("processing_modes")
    private List<String> processingModes = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPaymentTypeId() {
        return paymentTypeId;
    }

    public void setPaymentTypeId(String paymentTypeId) {
        this.paymentTypeId = paymentTypeId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSecureThumbnail() {
        return secureThumbnail;
    }

    public void setSecureThumbnail(String secureThumbnail) {
        this.secureThumbnail = secureThumbnail;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getDeferredCapture() {
        return deferredCapture;
    }

    public void setDeferredCapture(String deferredCapture) {
        this.deferredCapture = deferredCapture;
    }

    public List<Setting> getSettings() {
        return settings;
    }

    public void setSettings(List<Setting> settings) {
        this.settings = settings;
    }

    public List<String> getAdditionalInfoNeeded() {
        return additionalInfoNeeded;
    }

    public void setAdditionalInfoNeeded(List<String> additionalInfoNeeded) {
        this.additionalInfoNeeded = additionalInfoNeeded;
    }

    public Double getMinAllowedAmount() {
        return minAllowedAmount;
    }

    public void setMinAllowedAmount(Double minAllowedAmount) {
        this.minAllowedAmount = minAllowedAmount;
    }

    public Long getMaxAllowedAmount() {
        return maxAllowedAmount;
    }

    public void setMaxAllowedAmount(Long maxAllowedAmount) {
        this.maxAllowedAmount = maxAllowedAmount;
    }

    public Long getAccreditationTime() {
        return accreditationTime;
    }

    public void setAccreditationTime(Long accreditationTime) {
        this.accreditationTime = accreditationTime;
    }

    public List<Object> getFinancialInstitutions() {
        return financialInstitutions;
    }

    public void setFinancialInstitutions(List<Object> financialInstitutions) {
        this.financialInstitutions = financialInstitutions;
    }

    public List<String> getProcessingModes() {
        return processingModes;
    }

    public void setProcessingModes(List<String> processingModes) {
        this.processingModes = processingModes;
    }

    protected PaymentMethod(Parcel in) {
        id = in.readString();
        name = in.readString();
        paymentTypeId = in.readString();
        status = in.readString();
        secureThumbnail = in.readString();
        thumbnail = in.readString();
        deferredCapture = in.readString();
        if (in.readByte() == 0x01) {
            settings = new ArrayList<Setting>();
            in.readList(settings, Setting.class.getClassLoader());
        } else {
            settings = null;
        }
        if (in.readByte() == 0x01) {
            additionalInfoNeeded = new ArrayList<String>();
            in.readList(additionalInfoNeeded, String.class.getClassLoader());
        } else {
            additionalInfoNeeded = null;
        }
        minAllowedAmount = in.readByte() == 0x00 ? null : in.readDouble();
        maxAllowedAmount = in.readByte() == 0x00 ? null : in.readLong();
        accreditationTime = in.readByte() == 0x00 ? null : in.readLong();
        if (in.readByte() == 0x01) {
            financialInstitutions = new ArrayList<Object>();
            in.readList(financialInstitutions, Object.class.getClassLoader());
        } else {
            financialInstitutions = null;
        }
        if (in.readByte() == 0x01) {
            processingModes = new ArrayList<String>();
            in.readList(processingModes, String.class.getClassLoader());
        } else {
            processingModes = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(paymentTypeId);
        dest.writeString(status);
        dest.writeString(secureThumbnail);
        dest.writeString(thumbnail);
        dest.writeString(deferredCapture);
        if (settings == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(settings);
        }
        if (additionalInfoNeeded == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(additionalInfoNeeded);
        }
        if (minAllowedAmount == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeDouble(minAllowedAmount);
        }
        if (maxAllowedAmount == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeLong(maxAllowedAmount);
        }
        if (accreditationTime == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeLong(accreditationTime);
        }
        if (financialInstitutions == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(financialInstitutions);
        }
        if (processingModes == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(processingModes);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<PaymentMethod> CREATOR = new Parcelable.Creator<PaymentMethod>() {
        @Override
        public PaymentMethod createFromParcel(Parcel in) {
            return new PaymentMethod(in);
        }

        @Override
        public PaymentMethod[] newArray(int size) {
            return new PaymentMethod[size];
        }
    };
}

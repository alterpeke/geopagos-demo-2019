package com.example.geopagosdemo.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Issuer implements Parcelable {

    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("secure_thumbnail")
    private String secureThumbnail;

    @SerializedName("thumbnail")
    private String thumbnail;

    @SerializedName("processing_mode")
    private String processingMode;

    @SerializedName("merchant_account_id")
    private Object merchantAccountId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecureThumbnail() {
        return secureThumbnail;
    }

    public void setSecureThumbnail(String secureThumbnail) {
        this.secureThumbnail = secureThumbnail;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getProcessingMode() {
        return processingMode;
    }

    public void setProcessingMode(String processingMode) {
        this.processingMode = processingMode;
    }

    public Object getMerchantAccountId() {
        return merchantAccountId;
    }

    public void setMerchantAccountId(Object merchantAccountId) {
        this.merchantAccountId = merchantAccountId;
    }


    protected Issuer(Parcel in) {
        id = in.readString();
        name = in.readString();
        secureThumbnail = in.readString();
        thumbnail = in.readString();
        processingMode = in.readString();
        merchantAccountId = (Object) in.readValue(Object.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(secureThumbnail);
        dest.writeString(thumbnail);
        dest.writeString(processingMode);
        dest.writeValue(merchantAccountId);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Issuer> CREATOR = new Parcelable.Creator<Issuer>() {
        @Override
        public Issuer createFromParcel(Parcel in) {
            return new Issuer(in);
        }

        @Override
        public Issuer[] newArray(int size) {
            return new Issuer[size];
        }
    };
    
}

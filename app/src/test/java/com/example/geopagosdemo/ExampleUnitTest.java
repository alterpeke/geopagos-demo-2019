package com.example.geopagosdemo;

import com.example.geopagosdemo.business.IManager;
import com.example.geopagosdemo.business.PaymentMethodManager;
import com.example.geopagosdemo.model.PaymentMethod;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void check_paymentMethodService(){
        MainActivity mainActivity = new MainActivity();
        PaymentMethodManager manager = new PaymentMethodManager();
        manager.load(new IManager.IManagerLoadListener() {
            @Override
            public void onLoaded(ArrayList objectLoaded) {
                assertNotNull(objectLoaded);
            }

            @Override
            public void onError(int errorCode, String message) {
                System.console().printf("error ", message);
            }
        });
    }
}